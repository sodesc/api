from flask import Flask, request
import logging
import json
import random
from word import words

app = Flask(__name__)

logging.basicConfig(level=logging.INFO)

shots = [
    '1540737/aba36fb6dbb5a9abf058',
    '1030494/3c07149f3f9594087578',
    '1540737/ad630f49d0f9fd882ebb',
    '1540737/9049a15a6d4f48c8f616',
    '1540737/14ab1a6194d9765fce3f',
    '965417/bbc32193a650c840f937',
    '937455/a21e830c50df31e84fd5',
    '1652229/9e94d15cd578b1a3ebe1',
    '1030494/8869cac025429a169d21'
    ]


alphabet = ['а', 'б', 'в', 'г',
            'д', 'е', 'ё', 'ж',
            'з', 'и', 'й', 'к',
            'л', 'м', 'н', 'о',
            'п', 'р', 'с', 'т',
            'у', 'ф', 'х', 'ц',
            'ч', 'ш', 'щ', 'ъ',
            'ы', 'ь', 'э', 'ю',
            'я']

sessionStorage = {}


@app.route('/post', methods=['POST'])
def main():
    logging.info('Request: %r', request.json)
    response = {
        'session': request.json['session'],
        'version': request.json['version'],
        'response': {
            'end_session': False
        }
    }
    handle_dialog(response, request.json)
    logging.info('Response: %r', response)
    return json.dumps(response)


def get_letter(req):
    letter = req['request']['command'].lower()
    if letter in alphabet:
        return letter
    return None


def get_first_name(req):
    # перебираем сущности
    for entity in req['request']['nlu']['entities']:
        # находим сущность с типом 'YANDEX.FIO'
        if entity['type'] == 'YANDEX.FIO':
            # Если есть сущность с ключом 'first_name', то возвращаем её значение.
            # Во всех остальных случаях возвращаем None.
            return entity['value'].get('first_name', None)


def output_update(letter, output, word):
    s = ''
    for i in word:
        if (i == letter) or (i in output):
            s += i
        else:
            s += '*'
    return s


def handle_dialog(res, req):
    user_id = req['session']['user_id']
    if req['session']['new']:
        res['response']['text'] = 'Привет! Назови своё имя!'
        sessionStorage[user_id] = {
            'first_name': None,  # здесь будет храниться имя
            'game_started': False  # здесь информация о том, что пользователь начал игру. По умолчанию False
        }
        return

    if sessionStorage[user_id]['first_name'] is None:
        first_name = get_first_name(req)
        if first_name is None:
            res['response']['text'] = 'Не расслышала имя. Повтори, пожалуйста!'
        else:
            sessionStorage[user_id]['first_name'] = first_name
            # создаём пустой массив, в который будем записывать слова, которые пользователь уже отгадал
            sessionStorage[user_id]['guessed_words'] = []
            # как видно из предыдущего навыка, сюда мы попали, потому что пользователь написал своем имя.
            # Предлагаем ему сыграть и два варианта ответа "Да" и "Нет".
            res['response']['text'] = f'Приятно познакомиться, {first_name.title()}. Я Алиса. Сыграем в Виселицу?'
            res['response']['buttons'] = [
                {
                    'title': 'Да',
                    'hide': True
                },
                {
                    'title': 'Нет',
                    'hide': True
                }
            ]
    else:
        # У нас уже есть имя, и теперь мы ожидаем ответ на предложение сыграть.
        # В sessionStorage[user_id]['game_started'] хранится True или False в зависимости от того,
        # начал пользователь игру или нет.
        if not sessionStorage[user_id]['game_started']:
            # игра не начата, значит мы ожидаем ответ на предложение сыграть.
            if 'да' in req['request']['nlu']['tokens']:
                if len(sessionStorage[user_id]['guessed_words']) == len(words):
                    # если все слова отгаданы, то заканчиваем игру
                    res['response']['text'] = 'Ты отгадал все слова!'
                    res['end_session'] = True
                else:
                    # если есть неотгаданные слова, то продолжаем игру
                    sessionStorage[user_id]['game_started'] = True
                    # номер попытки, чтобы показывать фото по порядку
                    sessionStorage[user_id]['attempt'] = 1
                    sessionStorage[user_id]['step'] = 1
                    # функция, которая выбирает слово для игры и заменяет буквы/выдает картинки
                    play_game(res, req)
            elif 'нет' in req['request']['nlu']['tokens']:
                res['response']['text'] = 'Ну и ладно!'
                res['end_session'] = True
            else:
                res['response']['text'] = 'Не поняла ответа! Так да или нет?'
                res['response']['buttons'] = [
                    {
                        'title': 'Да',
                        'hide': True
                    },
                    {
                        'title': 'Нет',
                        'hide': True
                    }
                ]
        else:
            play_game(res, req)


def play_game(res, req):
    user_id = req['session']['user_id']     
    attempt = sessionStorage[user_id]['attempt']
    step = sessionStorage[user_id]['step']
    if step == 1:
        # если попытка первая, то случайным образом выбираем слово для гадания
        word = random.choice(words)
        sessionStorage[user_id]['guessed_letters'] = []
        # выбираем его до тех пор пока не выбираем слово, которого нет в sessionStorage[user_id]['guessed_words']
        while word in sessionStorage[user_id]['guessed_words']:
            word = random.choice(words)
        sessionStorage[user_id]['output'] = '*' * len(word)
        # записываем слово в информацию о пользователе
        sessionStorage[user_id]['word'] = word
        res['response']['text'] = 'Что это за слово?'
    else:
        # сюда попадаем, если попытка отгадать не первая
        word = sessionStorage[user_id]['word']
        letter = get_letter(req)
        # проверяем есть ли правильная буква в сообщение
        if letter:
            if letter in sessionStorage[user_id]['guessed_letters']:
                res['response']['text'] = 'Ты уже называл эту букву.'
            elif letter in word:
            # если да, то добавляем букву к sessionStorage[user_id]['guessed_letters']
                sessionStorage[user_id]['output'] = output_update(letter, sessionStorage[user_id]['output'], word)
                res['response']['text'] = 'Такая буква есть!'
                sessionStorage[user_id]['guessed_letters'].append(letter)
            else:
                res['response']['text'] = 'Такой буквы нет!'
                sessionStorage[user_id]['guessed_letters'].append(letter)
                sessionStorage[user_id]['attempt'] += 1
        else:
            res['response']['text'] = 'Какая-то неправильная буква. Попробуй ещё'

        if '*' not in sessionStorage[user_id]['output']:
            res['response']['text'] = 'Ты выиграл! (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧ヾ(⌐■_■)ノ♪'
            res['response']['text'] += 'Загаданное слово: {}. Сыграем ещё?'.format(word)
            sessionStorage[user_id]['guessed_words'].append(word)
            sessionStorage[user_id]['game_started'] = False
            res['response']['card'] = {}
            res['response']['card']['type'] = 'BigImage'
            res['response']['card']['title'] = res['response']['text']
            res['response']['card']['image_id'] = shots[sessionStorage[user_id]['attempt'] - 1]
            return
        elif sessionStorage[user_id]['attempt'] == 9:
            res['response']['text'] = 'Ты проиграл! ಠoಠ ¯\_(ツ)_/¯ '
            res['response']['text'] += 'Загаданное слово: {}. Сыграем ещё?'.format(word)
            sessionStorage[user_id]['guessed_words'].append(word)
            sessionStorage[user_id]['game_started'] = False
            res['response']['card'] = {}
            res['response']['card']['type'] = 'BigImage'
            res['response']['card']['title'] = res['response']['text']
            res['response']['card']['image_id'] = shots[sessionStorage[user_id]['attempt'] - 1]
            return
        
    attempt = sessionStorage[user_id]['attempt']
    res['response']['card'] = {}
    res['response']['card']['type'] = 'BigImage'
    res['response']['card']['title'] = res['response']['text'] + ' ' + sessionStorage[user_id]['output']
    res['response']['card']['image_id'] = shots[attempt - 1]
    sessionStorage[user_id]['step'] += 1
